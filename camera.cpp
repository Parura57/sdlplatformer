#include<SDL_image.h>
#include<unordered_map>

#include"camera.h"
#include"game.h"
#include"rendertools.h"

extern Game* game;

const int screenW = 640;
const int screenH = 480;

Camera::Camera(double x, double y) :
    GameObject::GameObject{ Vector2 {.x=x, .y=y}, Vector2 {.x=1.0, .y=1.0}, "", false } {
    // Initialize SDL and window
    if (SDL_Init(SDL_INIT_VIDEO) < 0) { printf("SDL could not initialize, Error: %s \n", SDL_GetError()); throw; }
    w = SDL_CreateWindow("Platfrom", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, screenW, screenH, SDL_WINDOW_SHOWN);
    if (w == NULL) { printf("Window could not be created, Error: %s \n", SDL_GetError()); throw; }
    render = SDL_CreateRenderer(w, -1, 0);
    if (render == NULL) { printf("Renderer could not be created, Error: %s \n", SDL_GetError()); throw; }
}

Camera::~Camera() {
    SDL_DestroyWindow(w);
}

void Camera::Render() {
    // Clear
    SDL_SetRenderDrawColor(render, 0x00, 0x00, 0x00, 0xFF);
    SDL_RenderClear(render);

    // Render
    SDL_Rect currentRect;
    for (auto const entry : game->GetObjects()) {
        auto obj = entry.second;
        currentRect = { static_cast<int>(obj->GetPosition().x - position.x), screenH - static_cast<int>(obj->GetPosition().y - position.y), static_cast<int>(obj->GetSize().x), static_cast<int>(obj->GetSize().y) };
        SDL_RenderCopy(render, textures[obj->GetSprite()], NULL, &currentRect);
    }

    SDL_RenderPresent(render);
}

void Camera::Update() {
    // Move towards the player or whatever is being tracked smoothly
    // To allow for a stationary camera without segfaulting
    if (following != nullptr) {
        position.x += ((following->GetPosition().x + following->GetSize().x) - screenW/2 - position.x) * 0.1;
        position.y += ((following->GetPosition().y + following->GetSize().y) - screenH/2 - position.y) * 0.1;
    }
}

void Camera::FinishInit() {
    // Set the camera to actually follow something, now that it is actually loaded
    following = game->GetObjects()[followingName];

    // Build the texture cache
    // We're not culling for duplicates here since it should already have been done in the surfaces step
    for (auto const entry : game->GetRessourceManager()->GetSurfaces()) {
        textures[entry.first] = SDL_CreateTextureFromSurface(render, entry.second);
    }
}

void Camera::SetFollowingName(std::string name) { followingName = name; }

