# SDL platformer game

A simple platforming game template/engine built using C++ with SDL2.

Development has been abandoned because the codebase is unfathomably spaghettified, currently there is a basic physics engine, character controls, loading levels and sprites from files, as well as unfinished animations in the sprites branch.

## Running

To run the game, clone the repository and run 

    $ make run

## Levels

Level data is stored in a .json, such that the top-level key is the name of the object, and its assigned value is a set of pairs defining position, and, if needed, width, height, textures, etc.

The types of objects implemented are:

### Camera

|Key|Field|
|---|-----|
|"x"|Horizontal position in pixels, double floating-point|
|"y"|Vertical position in pixels, double floating point|
|"following"|The name of the object the camera follows, usually the player. Optional|
|"type"|"Camera"|

### Player

|Key|Field|
|---|-----|
|"x"|Horizontal position in pixels, double floating-point|
|"y"|Vertical position in pixels, double floating point|
|"width"|Width in pixels, double floating point|
|"height"|Height in pixels, double floating point|
|"type"|"Player"|
|"sprite"|"./assets/sprites/player/player.png"|Path to the sprite|

### Platform

|Key|Field|
|---|-----|
|"x"|Horizontal position in pixels, double floating-point|
|"y"|Vertical position in pixels, double floating point|
|"width"|Width in pixels, double floating point|
|"height"|Height in pixels, double floating point|
|"type"|"Platform"|
|"sprite"|"./assets/sprites/ground/ground1.png"|Path to the sprite|

