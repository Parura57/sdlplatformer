#ifndef __RENDER_H
#define __RENDER_H

#include<SDL2/SDL.h>
#include<SDL2/SDL_image.h>
#include<string>
#include<vector>
#include<unordered_map>

// Caching sprites
class RessourceManager {
public:
    std::unordered_map<std::string, SDL_Surface*> GetSurfaces();
    void CreateSurface(std::string name, std::string path);

private:
    std::unordered_map<std::string, SDL_Surface*> surfaces;
    std::vector<std::string> loadedPaths;
};

#endif

