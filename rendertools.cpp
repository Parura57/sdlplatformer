#include"rendertools.h"

// TODO This doesn't actually prevent anything from loading twice, it should look up by path and not by name so probably a vector of already loaded paths?
void RessourceManager::CreateSurface(std::string name, std::string path) {
    if (surfaces.find(name) == surfaces.end()) {
        SDL_Surface* surf = IMG_Load(path.c_str());
        surfaces[name] = surf;
    }
}

std::unordered_map<std::string, SDL_Surface*> RessourceManager::GetSurfaces() { return surfaces; }

