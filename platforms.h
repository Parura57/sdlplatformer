#ifndef __PLATFORM_H
#define __PLATFORM_H

#include<string>

#include"gameObjects.h"

class Platform : public GameObject {
public:
    Platform(double x, double y, double width, double height, std::string sprite);
    ~Platform();

private:
    int width, height;
    float friction;
};

#endif

