#include<stdio.h>
#include<unistd.h>
#include<chrono>
#include<thread>
#include<stdexcept>

#include"game.h"

/* Main return codes
 * 0: Success
 * 1: Initialising failure
 * 2: Update/Render failure
 */

// This has to be in the main file or such that it is not ever #included circularly because yes apparently
Game* Game::instancePtr = NULL;
// TODO THIS IS NOT HOW YOU USE A SINGLETON
Game* game;

int main(int argc, char *argv[]) {
    int returncode = 0;
    game = Game::GetInstance();
    printf("Initialised game\n");
    game->LoadLevel("assets/levels/level1.json");
    printf("Loaded level\n");

    // Main loop
    SDL_Event event;
    SDL_PumpEvents();
    auto frameStarted = std::chrono::high_resolution_clock::now();
    auto frameDone = std::chrono::high_resolution_clock::now();
    auto frametime = std::chrono::duration_cast<std::chrono::milliseconds>(frameDone-frameStarted);
    const std::chrono::milliseconds wantedFrametime{16};
    while (game->isRunning()) {
    frameStarted = std::chrono::high_resolution_clock::now();
        while (SDL_PollEvent(&event)) {
            game->EventHandler(event);
            SDL_PumpEvents();
        }
        try {
            game->Update();
            game->Render();
        } catch (const std::exception& e) {
            printf("Error updating/rendering: %s\n", e.what());
            returncode = 2;
            break;
        }
        //printf("frame\n");
        frameDone = std::chrono::high_resolution_clock::now();
        frametime = std::chrono::duration_cast<std::chrono::milliseconds>(frameDone-frameStarted);
        std::this_thread::sleep_for(wantedFrametime - frametime);
    }
    // Exit
    SDL_Quit();
    return returncode;
}

