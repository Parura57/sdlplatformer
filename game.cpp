#include<stdio.h>
#include<unistd.h>
#include<fstream>
#include<string>
#include<vector>
#include<iostream>

#include"game.h"
#include"rendertools.h"

Game::Game() {
    values.gameRunning = true;
    ressourceManager = new RessourceManager;
}

Game::~Game() {
}

Game* Game::GetInstance() {
    if (instancePtr == NULL) instancePtr = new Game(); 
    return instancePtr; 
return 0;
}

void Game::LoadLevel(std::string path) {
    // TODO Add a placeholder if a sprite is not defined
    // Load level
    std::ifstream fd("assets/levels/level1.json");
    level = nlohmann::json::parse(fd);
    for (auto it = level.begin(); it != level.end(); ++it) {
        // Because it.key() is a char*
        std::string key = it.key();
        std::cout << "key: " << key << ", value:" << it.value() << '\n';
        if (it.value()["type"] == "Camera") {
            gameObjects[key] = new Camera(it.value()["x"], it.value()["y"]);
            std::string a = it.value()["following"];
            dynamic_cast<Camera*>(gameObjects[key])->SetFollowingName(a);
        }
        else if (it.value()["type"] == "Player") {
            gameObjects[key] = new Player(it.value()["x"], it.value()["y"], it.value()["width"], it.value()["height"], it.key());
        }
        else if (it.value()["type"] == "Platform") {
            gameObjects[key] = new Platform(it.value()["x"], it.value()["y"], it.value()["width"], it.value()["height"], it.key());
        }

        // Load sprites
        if (it.value().contains("sprite")) {
            ressourceManager->CreateSurface(it.key(), it.value()["sprite"]);
        }
    }

    for (auto const& i : gameObjects) {
        // The cast will fail if the object is not a camera
        dynamic_cast<Camera*>(gameObjects["camera"])->FinishInit();
    }
}

void Game::EventHandler(SDL_Event e) {
    switch(e.type) {
    case SDL_QUIT: 
        values.gameRunning = false;
        break;

    case SDL_KEYDOWN:
        switch (e.key.keysym.scancode) {
        case SDL_SCANCODE_LEFT: keysPressed = keysPressed | 0b00100000; break;
        case SDL_SCANCODE_RIGHT: keysPressed = keysPressed | 0b00010000; break;
        case SDL_SCANCODE_UP: keysPressed = keysPressed | 0b10000000; break;

        case SDL_SCANCODE_ESCAPE: values.gameRunning = false; break;
        }
        break;

    case SDL_KEYUP:
        switch (e.key.keysym.scancode) {
        case SDL_SCANCODE_LEFT: keysPressed = keysPressed ^ 0b00100000; break;
        case SDL_SCANCODE_RIGHT: keysPressed = keysPressed ^ 0b00010000; break;
        case SDL_SCANCODE_UP: keysPressed = keysPressed ^ 0b10000000; break;
        }
        break;
    }
}

void Game::Update() {

    for (const std::pair<const std::string, GameObject*>& i : gameObjects) {
        i.second->Update();
    }
}

void Game::Render() {
    for (auto const& i : gameObjects) {
        Camera* camera = dynamic_cast< Camera* >(i.second);
        if (camera == nullptr) continue;
        camera->Render();
        break;
    }
}

void Game::Exit() {
    sleep(2);
    values.gameRunning = false;
}

Game::GameValues Game::GetValues() { return values; }
unsigned char Game::GetKeys() { return keysPressed; }
bool Game::isRunning() { return values.gameRunning; }
std::unordered_map<std::string, GameObject*> Game::GetObjects() { return gameObjects; }
RessourceManager* Game::GetRessourceManager() { return ressourceManager; }

