#include "player.h"
#include "game.h"

// Ew
extern Game* game;

// The player is not solid, we don't want it to collide with itself
// If/Once enemies are added, we'll probably rewrite this to use masks of some description
Player::Player(double x, double y, double width, double height, std::string sprite) :
    GameObject::GameObject{ Vector2 {.x=x, .y=y}, Vector2 {.x=width, .y=height}, sprite, false } {
    canJump = maxJumps;
    jumpCooldown = false;
}

Player::~Player() {}

void Player::Update() {
    // Key handling
    if (active) {
        // Left-right
        if (game->GetKeys() & 0b00100000) velocity.x = -xAccel;
        if (game->GetKeys() & 0b00010000) velocity.x = xAccel;
        // Jump
        if (game->GetKeys() & 0b10000000) { if (canJump && !jumpCooldown) {
            velocity.y = yAccel;
            canJump--;
            jumpCooldown = true;
        }}
        else jumpCooldown = false;
        position.x += velocity.x;
        position.y += velocity.y;
    }

    // Gravity
    velocity.y -= game->GetValues().gravity;

    // Death
    if (position.y < 0) {
        velocity.x = 0.0;
        velocity.y = 0.0;
        // TODO Semi-temporary
        active = false;
        game->Exit();
    }

    // Air friction
    velocity.x -= game->GetValues().airFriction * velocity.x;

    // Physcs collision
    for (auto const& entry : game->GetObjects()) {
        auto obj = *entry.second;
        if (!obj.IsActive()) continue;
        if (position.x + size.x >= obj.GetPosition().x &&
            position.x <= obj.GetPosition().x + obj.GetSize().x &&
            position.y >= obj.GetPosition().y - obj.GetSize().y &&
            position.y - size.y <= obj.GetPosition().y) {

            // FTR Im not changing this even after refactoring 2/3rds of the code, it works
            // Check for whether the collision is from the top or the sides
            // From the right
            if (position.x - 2*velocity.x >= obj.GetPosition().x + obj.GetSize().x) {
                HandlePlatforms(obj, 0);
            }   // From the top
            if (position.y - size.y - 2*velocity.y >= obj.GetPosition().y) {    // That 2*velocity is hacky but it works
                HandlePlatforms(obj, 1);
            }   // From the left
            if (position.x + size.x - 2*velocity.x <= obj.GetPosition().x) {
                HandlePlatforms(obj, 2);
            }   // From the bottom
            if (position.y - 2*velocity.y <= obj.GetPosition().y - obj.GetSize().y) {
                HandlePlatforms(obj, 3);
            }
        }
    }
}

// We are assuming that anything we come across behaves as a platform
void Player::HandlePlatforms(GameObject obj, unsigned char direction) {
    Game* game = Game::GetInstance();

    // 0 is right, rotating counterclockwise
    switch (direction) {
    case 0:     // Right
        position.x = obj.GetPosition().x + obj.GetSize().x;
        if (velocity.x < 0) velocity.x = 0;
        break;
    case 1:     // Up
        position.y = obj.GetPosition().y + size.y;
        if (velocity.y < 0) velocity.y = 0;     // So that we don't completely block jumping
        velocity.x -= game->GetValues().friction * velocity.x;
        canJump = maxJumps;
        break;
    case 2:     // Left
        position.x = obj.GetPosition().x - size.x;
        if (velocity.x > 0) velocity.x = 0;
        break;
    case 3:     // Down
        position.y = obj.GetPosition().y - obj.GetSize().y;
        if (velocity.y > 0) velocity.y = 0;
        break;
    default:
        printf("Invalid direction: %u\n", direction);
    }
}

