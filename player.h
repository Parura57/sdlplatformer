#ifndef __PLAYER_H
#define __PLAYER_H

#include<string>

#include"gameObjects.h"

class Player : public GameObject {
public:
    Player(double x, double y, double width, double height, std::string sprite);
    ~Player();
    void Update();

private:
    const double xAccel = 10;
    const double yAccel = 20;
    const unsigned char maxJumps = 2;
    unsigned char canJump;
    bool jumpCooldown;

    void HandlePlatforms(GameObject obj, unsigned char direction);
};

#endif

