#ifndef __GAME_H
#define __GAME_H

#include<SDL2/SDL.h>
#include<vector>
#include<string>
#include<unordered_map>
#include<nlohmann/json.hpp>

#include"gameObjects.h"
#include"rendertools.h"
#include"player.h"
#include"camera.h"
#include"platforms.h"

class Game {
private:
    static Game* instancePtr;

    Game();
    ~Game();

    unsigned char keysPressed;
    struct GameValues {
        const double gravity = 0.8;
        const double friction = 0.3;
        const double airFriction = 0.1;
        unsigned char mode = 0b00000000;
        bool gameRunning = false;
    } values;

    std::unordered_map<std::string, GameObject*> gameObjects;
    nlohmann::json level;

    RessourceManager* ressourceManager;

public:
    Game(const Game& obj) = delete;

    static Game* GetInstance();
    GameValues GetValues();
    unsigned char GetKeys();
    bool isRunning();
    std::unordered_map<std::string, GameObject*> GetObjects();
    RessourceManager* GetRessourceManager();

    void LoadLevel(std::string path);
    void EventHandler(SDL_Event e);
    void Update();
    void Render();
    void Exit();
};

#endif

