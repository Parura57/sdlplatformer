#ifndef __GAMEOBJECTS_H
#define __GAMEOBJECTS_H

#include<SDL2/SDL.h>
#include<string>
#include<vector>

#include"rendertools.h"

struct Vector2 {
    double x;
    double y;
};

class GameObject {
public:
    GameObject(Vector2 position, Vector2 size, std::string sprite, bool solid);
    ~GameObject();

    virtual void Update() {};

    Vector2 GetPosition();
    Vector2 GetVelocity();
    Vector2 GetSize();
    bool IsActive();
    std::string GetSprite();
    bool IsSolid();

    void SetPosition(Vector2 pos);
    void SetVelocity(Vector2 vel);

protected:
    bool active;
    Vector2 position;
    Vector2 velocity;
    Vector2 size;
    std::string sprite;
    // Solid here just means it is visible to the player collision handler
    bool solid;
};

#endif

