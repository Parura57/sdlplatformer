#ifndef __CAMERA_H
#define __CAMERA_H

#include<string>

#include"gameObjects.h"

class Camera : public GameObject {
public:
    Camera(double x, double y);
    ~Camera();
    void Render();
    void Update();

    void SetFollowingName(std::string name);
    void FinishInit();

private:
    SDL_Window* w;
    SDL_Renderer* render;
    // To follow the player
    GameObject* following = nullptr;
    std::string followingName;
    std::unordered_map<std::string, SDL_Texture*> textures;
};

#endif
