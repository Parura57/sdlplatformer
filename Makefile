CXX=$(shell which g++)
INCLUDE=$(shell pkg-config --cflags --libs sdl2 SDL2_ttf SDL2_image)
CXXFLAGS=-O2

TARGETS=rendertools.o gameObjects.o player.o camera.o platforms.o game.o

all: clean ${TARGETS}
	${CXX} main.cpp -o platformer ${INCLUDE} ${CXXFLAGS} ${TARGETS}

%.o : %.cpp
	${CXX} -c $< -o $@ ${INCLUDE} ${CXXFLAGS}

clean:
	rm -f platformer *.o

devclean:
	rm -f $(forcebuild)

dev: devclean ${TARGETS}
	${CXX} main.cpp -o platformer -g3 ${INCLUDE} ${CXXFLAGS} ${TARGETS}

.PHONY: all clean devclean dev run
