#include"gameObjects.h"

GameObject::GameObject(Vector2 position, Vector2 size, std::string sprite, bool solid) :
    position(position), size(size), sprite(sprite), solid(solid) {
    active = true;
    velocity = Vector2 { .x=0, .y=0 };
}

GameObject::~GameObject() {}

Vector2 GameObject::GetPosition() { return position; }
Vector2 GameObject::GetVelocity() { return velocity; }
Vector2 GameObject::GetSize() { return size; }
std::string GameObject::GetSprite() { return sprite; }
bool GameObject::IsActive() { return active; }
bool GameObject::IsSolid() { return solid; }

void GameObject::SetPosition(Vector2 pos) { position = pos; }
void GameObject::SetVelocity(Vector2 vel) { velocity = vel; }

